# c++ miscellaneous libraries

This project is to write by myself libraries (even though there are
plenty out there, like boost) as a way to keep simple usual things we
use in every day programming and to learn how to do it. This
self-learning project was started when the C++11 support wasn't too
spread (in my g++ version the C++11 flag was still used as -std=c++0x)


## Libraries

At this point I have:

- [idioms](/lemniscata/c-libraries/src/master/idioms): useful and
  common c++ constructions.

- [ipointers](/lemniscata/c-libraries/src/master/ipointers):
  Intelligent pointers library.

- [metaprogramming library](/lemniscata/c-libraries/src/master/mpl)

- [patterns](/lemniscata/c-libraries/src/master/patterns): common
  programming patterns.

- [sockets](/lemniscata/c-libraries/src/master/sockets): socket
  utilities for efficient communication.

- [threads](/lemniscata/c-libraries/src/master/threads): Thread
  creation and manipulation utilities.

- [timer](/lemniscata/c-libraries/src/master/timer): Time retrieving
  and counting utilities.

- [utils](/lemniscata/c-libraries/src/master/utils): diverse utilities
  for general programming.

	- [stringutils](/lemniscata/c-libraries/master/utils/stringutils):
	  String manipulation utilities.

	- [functors](/lemniscata/c-libraries/master/utils/functors)


## How to use them

Most of them come in the form of source code that can be compiled to a
static or a dynamic library and then linked to it, or add sources
directly to projects.

A makefile is available in each one to build libraries.

A libflag tool is available in the root to link to each one of the libraries.
Just use ```libflag --include --lib XXX+``` where XXX+ are the names of the
libraries you want to link against.

To include a header into one of your sources using our tool you should do it:

	#include "foldername/headername.hpp"
