/// \brief Utilities to easy creation of objects. 
/// It develops some classes that can be used to easy the task of
/// creating new operator from scratch.

/// This file depends in idioms/functors.cpp file
///                      utils/functors/functors.hpp


#ifndef __CREATOR_HPP__
#define __CREATOR_HPP__


#include <cstdlib>
#include <new>
#include <iostream>

#include <utils/functors/functors.hpp>
#include <idioms/thrower.hpp>

namespace idioms{

/** Definition of the type that std::new uses as a handler */
typedef void (*new_handler)();

/** The default handler used with safenew class. It shows a msg and
    aborts */
void default_handler();

    
/**
   class intended to customize new bad_alloc handling with a
   customized function or functor. The way of using is to inherit from
   it privately:
   
   	class X :private safenew<X> { ... }

   and then set your own handler for the class:

   	X::set_new_handler(handler);

   It uses a policy class, throwPolicy to handle the throwing resolution.
*/
template < class T , class throwPolicy=thrower >
class safenew :public throwPolicy{

    /** Policy class to handle throwing */
    using throwPolicy::throw_exception; 

public:
    template <class U> static functor set_new_handler(U h); 
    static void* operator new (size_t size);
    static void* operator new[] (size_t size);
	
    //private:
    
    static void entry_point();
    static functor _handler;
};


} //namespace idioms


#include "creator-inl.hpp"

#endif
