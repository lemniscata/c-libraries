#ifndef __THROWER_HPP__
#define __THROWER_HPP__

namespace idioms{

/** Policy class for throwing exception in the normal way*/
class thrower{
protected:    

    template <class T>
    static void throw_exception(const T& excp){
	throw excp;
    }

};


/** Policy class for not-throwing exception */
class no_thrower{
protected:    

    template <class T>
    static void throw_exception(const T& excp){}

};


} // namespace idioms

#endif //__THROWER_HPP__
