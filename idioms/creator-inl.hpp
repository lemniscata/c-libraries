/// \brief Implementation of methods defined in creator.hpp file.
/// This file is included later in creator.hpp and is used to separete
/// implementation from definition.

#ifndef __CREATOR_INL_HPP__
#define __CREATOR_INL_HPP__

#ifdef __CREATOR_HPP__

#include <iostream>

namespace idioms{

void default_handler(){
    std::cerr << "No more memory to allocate..." << std::endl;
    abort();
}


//  [begin] class safenew<T,throwPolicy> ==

template <class T,class throwPolicy>
functor safenew<T,throwPolicy>::_handler=functor(default_handler);
    

template <class T,class throwPolicy>
void* safenew<T,throwPolicy>::operator new(size_t size){
    new_handler _h= _handler ? entry_point:NULL;
    
    new_handler global=
	std::set_new_handler(_h);

    void* memory;
    try{
	memory= ::operator new(size);
    }
    catch(std::bad_alloc& ex){
	std::set_new_handler(global);
	throw_exception(ex);
    }

    std::set_new_handler(global);
    return memory;	
}

template <class T,class throwPolicy>
void* safenew<T,throwPolicy>::operator new[] (size_t size){
    new_handler _h= _handler ? entry_point:NULL;

    std::cout<< _h << std::endl;

    new_handler global=
	std::set_new_handler(_h);

    void* memory;
    try{
	memory= ::operator new(size);
    }
    catch(std::bad_alloc& ex){
	std::set_new_handler(global);
	throw_exception(ex);
    }

    std::set_new_handler(global);
    return memory;	
}


template <class T,class throwPolicy>
template <class U>
functor safenew<T,throwPolicy>::set_new_handler(U h){
    functor old=_handler;
    _handler=functor(h);
    return old;
}


template <class T,class throwPolicy>
void safenew<T,throwPolicy>::entry_point(){
    _handler();
}

// [end] class safenew<T,throwPolicy>


} // namespace idioms


#endif // __CREATOR_HPP__

#endif // __CREATOR_INL_HPP__
