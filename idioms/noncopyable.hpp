#ifndef __NONCOPYABLE_HPP__
#define __NONCOPYABLE_HPP__

template <class T>
class noncopyable{
protected:
    noncopyable(){}
    ~noncopyable(){}

private:
    noncopyable(const noncopyable&);
    T& operator=(const T&);
};

#endif // __NONCOPYABLE_HPP__
