

template <class T>
struct type_wrapper{
    typedef T type;
};

template<class R>
struct extract_type;

template <class R>
struct extract_type<R (*)(void)>{
    typedef R type;
};



int asfd();

int main(){
    extract_type<asfd>::type;
    // a=6;
    

    return 0;
}
