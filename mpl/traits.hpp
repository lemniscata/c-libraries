#ifndef __TRAITS_HPP__
#define __TRAITS_HPP__

// [begin] equals idiom

template <class D,class B>
struct both_equals{
    static const bool value=false;
};

template <class B>
struct both_equals<B,B>{
    static const bool value=true;
};

// [end] equals idiom



/**
   class that implements the traits of a class B in the form traits<B>
 */
template <class B>
class traits{
public:

    /**
       Equality trait, traits<B>::equals<D>::value==true <-> B==D
     */
    template <class D>
    struct equals{
	static const bool value=both_equals<B,D>::value;
    }; // class equals<D>
    

    /**
       Base of trait:
       	traits<B>::is_base_of<D>::value==true <-> D is polimorfically B
     */
    template <class D>
    class is_base_of{
	typedef char yes[1];
	typedef char no[2];
	
	static yes& test(B*);
	static no& test(...);

	static D* get(void);

    public:
	static const bool value=(sizeof(test(get()))==sizeof(yes));
    }; // class is_base_of<D>

    /**
       Strict base trait:
       	traits<B>::is_strict_base_of<D>::value==true <->
	D inherits from B
     */
    template <class D>
    struct is_strict_base_of{
	static const bool value=traits<B>::is_base_of<D>::value &&
	    !traits<B>::equals<D>::value;
    }; // class is_strict_base_of<D>

}; // class traits<B>


#define IS_DERIVED(base,derived) traits<base>::is_base_of<derived>::value

#define IS_STRICTLY_DERIVED(base,derived) \
    traits<base>::is_base_of<derived>::value

#define EQUALS(base,other) traits<base>::equals<other>::value 

#endif // __TRAITS_HPP__
