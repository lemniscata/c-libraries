#include <iostream>

using namespace std;


template <class B, class BPtr>
struct is_ptr_to{
    typedef char yes[2];
    typedef char no[1];

    static yes& test(B*);
    static no& test(...);

    static BPtr get(void);

    static const bool value= sizeof(test(&(*get())))==sizeof(yes);
};



template<class T>
struct is_bare_ptr{
    static const bool value=false;
};

template <class T>
struct is_bare_ptr<T*>{
    static const bool value=true;
};

template <class T>
struct has_ptr_operator{

    typedef char yes[2];
    typedef char no[1];

    
    template <class C> static yes& test(decltype(&C::operator*));
    template <class C> static no& test(...);

    static const bool value=sizeof(test<T>(0)) == sizeof(yes);

};

class A{
 public:
    A* operator*(){
	return this;
    }
};

class B{

};

int main(){
    cout << is_ptr_to<int,char*>::value << endl;

    cout << is_bare_ptr<int>::value<<endl;
    cout << is_bare_ptr<int*>::value<<endl;


    cout << endl;
    cout << has_ptr_operator<A>::value << endl;
    cout << has_ptr_operator<B>::value << endl;

    return 0;
}
