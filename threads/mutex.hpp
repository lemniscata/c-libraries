// Header file for mutex pthread API wrapper. It implements usual
// mutex functionality as well as exclusive ownership.

//Author: Pablo Cabeza García

#ifndef __MUTEX_HPP__
#define __MUTEX_HPP__

class condition;

//TODO set it with strict ownership to be able to copy it
/**
 * Mutual exclusion region wrap from pthread API. It implements strict
 * ownership, so only one mutex class will represent it. To pass it
 * you should use references or pointers.
 */
class mutex{
public:
    mutex();
    //mutex(mutex & m);
    virtual ~mutex();
    
    //mutex & operator=(mutex & m);

    bool lock();
    bool trylock();
    bool unlock();
    
private:
    //void free();

    friend class condition;
    
    struct mutex_t; /**< Declaration of mutex_t, the actual mutex */
    mutex_t * _mut;
};

#endif //__MUTEX_HPP__
