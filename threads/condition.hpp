#ifndef __CONDITION_HPP__
#define __CONDITION_HPP__

class mutex; //Forward declaration of mutex

//TODO make it as a shared_ptr
class condition{
public:
    condition();
    //condition(const condition & ref);
    virtual ~condition();

    //condition& operator=(const condition & ref);

    void wait(mutex &m);
    void signal();
    void broadcast();

private:
    //void free();
    //int * _count;

    struct cond_t; /**< Declaration of condition_t, the actual condition */
    cond_t * _cond;
};
#endif //__CONDITION_HPP__
