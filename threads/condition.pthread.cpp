// TODO as it is now is not copy safe.

// Author: Pablo Cabeza García

#include "condition.hpp"
#include "mutex.hpp"
#include "types.pthread.hpp"

condition::condition():_cond(new cond_t){
    pthread_cond_init(&_cond->c,NULL);
}

    
condition::~condition(){
    pthread_cond_destroy(&_cond->c);
    delete _cond;
}


void condition::wait(mutex &m){
    pthread_cond_wait(&_cond->c,&m._mut->m);
}

    
void condition::signal(){
    pthread_cond_signal(&_cond->c);
}


void condition::broadcast(){
    pthread_cond_broadcast(&_cond->c);
}
