// Definition of methods defined in thread.hpp. This header should only
// be included for within it, and is guarded for that

// Author: Pablo Cabeza García


#ifndef __THREAD_INL_HPP__
#define __THREAD_INL_HPP__

#ifdef __THREAD_HPP__ //Just include this if it's from thread.hpp

#include "types.pthread.hpp" //types for pthread API implementation

#include <pthread.h>


template <class T>
thread<T>::thread():_init(false),_this(new thread_t){};

template <class T>
thread<T>::thread(const T &e):_init(true),_exec(e),_this(new thread_t){};

template <class T>
thread<T>::~thread(){
    delete _this;
}



template <typename T>
int thread<T>::start(){
    if(_init) return pthread_create( &_this->t,NULL, tcallback,this);
    else return -1;
}


template <typename T>
void thread<T>::join(){
    if(_init) pthread_join(_this->t,NULL);
    else return -1;
}


template <typename T>
void thread<T>::exit(){
    pthread_exit(NULL);
}


template <typename T>
void * thread<T>::tcallback(void * This){
    static_cast<thread*>(This)->_exec();
    return NULL;
}

#endif //__THREAD_HPP__

#endif //__THREAD-INL_HPP__
