// All-in-one header file for the thread library. It includes all other
// relevants headers. It could also be used one by one.

//Author: Pablo Cabeza García

#ifndef __THREADS_ALL_HPP__
#define __THREADS_ALL_HPP__

#include "thread.hpp"
#include "mutex.hpp"
#include "condition.hpp"

#endif //__THREADS_ALL_HPP__
