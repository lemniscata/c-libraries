//

#ifndef __THREAD_HPP__
#define __THREAD_HPP__


/**
 * thread class represents a thread. It wraps pthread API thread in a class
 * style to be used easily in c++ developing. To be runned you need to pass
 * to it a class with operator() that can be called without parameters.
 */
template<typename T>
class thread{
public:
    thread();
    explicit thread(const T &e);

    virtual ~thread();

    virtual inline int start();
    inline void join();
    static inline void exit();

private:
    static inline void * tcallback(void * This);

    bool _init;
    T _exec;
    
    struct thread_t; /**< Forward declaration of thread_t */
    thread_t * _this;
};

#include "thread-inl.pthread.hpp"

#endif //__THREAD_HPP__
