// Wrapper of std::queue, it depends on mutex.hpp and condition.hpp to be
// there in order to work.
 
// Author: Pablo Cabeza García

#ifndef __BQUEUE_HPP__
#define __BQUEUE_HPP__

#include "mutex.hpp"
#include "condition.hpp"

#include <queue>

/**
 * bqueue is a std::queue wrapper for a blocking queue, meaning that it
 * will block the thread trying to pop() if queue.empty(). 
 */
template <class T>
class bqueue{
public:

    inline void push(const T& elem);
    inline T pop();
    inline bool empty();

private:
    mutex _m;
    condition _c;

    std::queue<T> _internal;
};

#include "bqueue-inl.hpp"

#endif //__BQUEUE_HPP__
