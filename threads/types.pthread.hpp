// Header file that defines types for pthread wrapper implementation.
// It is done this way to extend it to any other systems threads.

// Author: Pablo Cabeza García

#ifndef __TYPES_PTHREAD_HPP__
#define __TYPES_PTHREAD_HPP__

#include "mutex.hpp"
#include "condition.hpp"
#include "thread.hpp"

#include <pthread.h>

/** Holder for pthread API mutex */
struct mutex::mutex_t{
    pthread_mutex_t m;
};


/** Holder for pthread API conditional mutex objects */
struct condition::cond_t{
    pthread_cond_t c;
};
 
template <class T>
struct thread<T>::thread_t{
    pthread_t t;
};

#endif //__TYPES_PTHREAD_HPP__
