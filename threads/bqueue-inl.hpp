// Implementation of header bqueue.hpp

//Author: Pablo Cabeza García

#ifndef __BQUEUE_INL_HPP__
#define __BQUEUE_INL_HPP__

#ifdef __BQUEUE_HPP__ //Just include if bqueue.hpp is included

template <class T>
void bqueue<T>::push(const T& elem){
    _m.lock(); //Begin block
	
    _internal.push(elem);
    _c.signal(); //Signal that there is a new elem
	
    _m.unlock(); //End block
}


template <class T>
T bqueue<T>::pop(){
    _m.lock(); //Begin block
    
    while(_internal.empty()) //Guard from spurous interrupts
	_c.wait(_m); 

    T elem=_internal.front();
    _internal.pop();
	
    _m.unlock(); // End block
	
    return elem;
}


template <class T>
bool bqueue<T>::empty(){
    _m.lock();
    bool empty=_internal.empty();
    _m.unlock();
    
    return empty;    
}


#endif //__BQUEUE_HPP__

#endif //__BQUEUE_INL_HPP__
