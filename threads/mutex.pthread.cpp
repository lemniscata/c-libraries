// Mutex implementaions using pthread api to implement it
// TODO as it is now is not copy safe

// Author: Pablo Cabeza García

#include "mutex.hpp"
#include "types.pthread.hpp"

#include <pthread.h>



mutex::mutex():_mut(new mutex_t){
    pthread_mutex_init(&_mut->m,NULL);
}


mutex::~mutex(){
    pthread_mutex_destroy(&_mut->m);
}


bool mutex::lock(){
    return !pthread_mutex_lock(&_mut->m);
}


bool mutex::trylock(){
    return !pthread_mutex_trylock(&_mut->m);
}


bool mutex::unlock(){
    return !pthread_mutex_unlock(&_mut->m);
}
