#ifndef __SINGLETON_HPP__
#define __SINGLETON_HPP__


template <class T>
class DefCreator{
public:
    static T* create_instance(){
	return new T;
    }
};

/**
   This class acts as a singleton factory for objects instanciated 
 */
template <class T,class Creator=DefCreator<T> >
class singleton{
public:

    typedef T type;

    static T& instance(){
	static T instance=*Creator::create_instance();
	return instance; 
    }

private:
    singleton();
    singleton(const singleton&);
    singleton& operator= (const singleton&); 
};

#define SINGLETON(name) typedef singleton<name> name##Singleton

#endif //__SINGLETON_HPP__
