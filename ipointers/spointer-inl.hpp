// Actual implementation of shared_ptr template. It is included
// in upointer.hpp and should only be used from there.

// Author: Pablo Cabeza García

#ifndef __SPOINTER_INL_HPP__
#define __SPOINTER_INL_HPP__

#ifdef __SPOINTER_HPP__


template <class T,class D>
shared_ptr<T,D>::shared_ptr()
    :_ptr(NULL),
     _count(NULL){}


template <class T, class D>
shared_ptr<T,D>::shared_ptr(const shared_ptr<T,D> & ptr)
    :_ptr(ptr._ptr),
     _count(ptr._count){
    
    if(_count != NULL)(*_count)++;
}


template <class T, class D>
shared_ptr<T,D>::shared_ptr(T* ptr)
    :_ptr(ptr),
     _count((ptr!=NULL)?new int:NULL){

    if(_count!=NULL)*_count=1;
}


template <class T, class D>
shared_ptr<T,D>::shared_ptr(unique_ptr<T,D>& ptr)
    :_ptr(ptr._ptr),
     _count((ptr!=NULL)?new int:NULL){

    if(_count!=NULL)*_count=1;

    ptr._ptr=NULL;
}


template <class T, class D>
shared_ptr<T,D>::~shared_ptr(){
    free();
}


template <class T, class D>
shared_ptr<T,D>& shared_ptr<T,D>::operator=(const shared_ptr<T,D>& other){
    free();

    _count=other._count;
    _ptr=other._ptr;

    if(_count!=NULL) (*_count)++;

    return *this;
}


template <class T, class D>
T& shared_ptr<T,D>::operator*() const{
    return *_ptr;
}


template <class T, class D>
T* shared_ptr<T,D>::operator->() const{
    return _ptr;
}


template <class T, class D>
shared_ptr<T,D>::operator bool() const{
    if(_ptr==NULL) return false;
    return true;
}

template <class T, class D>
shared_ptr<T,D>& shared_ptr<T,D>::realease(){
    free();
    return *this;
}
    

template <class T, class D>
const T* shared_ptr<T,D>::get() const{
    return _ptr;
}


template <class T, class D>
void shared_ptr<T,D>::free(){
    if(_ptr!=NULL){
	if(*_count == 1){
	    // D deletor;
	    //deletor(_ptr);
	    delete _ptr;
	    delete _count;
	}
	else (*_count)--;
    }
    
    _ptr=NULL;
    _count=NULL;
}


#endif //__SPOINTER_HPP__

#endif //__SPOINTER_INL_HPP__
