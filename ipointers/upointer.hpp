/// \brief Header only unique_ptr template.
/// It defines an intelligent pointer with strict ownership.
/// This file only has definitions, the actual implementation 
/// is in upointer-inl.hpp file, that is included

/// \author: Pablo Cabeza García

#ifndef __UPOINTER_HPP__
#define __UPOINTER_HPP__

//Definition of NULL in case it is no defined
#ifndef NULL
  #define NULL 0
#endif

#include "iptrfwd.hpp"
#include "deleter.hpp"

/**
 * unique_ptr class defines an intelligent pointes with strict
 * ownership, all major pointer operators are defined for this class.
 */
template<class T,class D>
class unique_ptr{
public:    
    unique_ptr(); /**< Default constructor, sets everything to null */
    unique_ptr(unique_ptr<T,D>& ptr);  /**< Copy constructor */
    explicit unique_ptr(T* ptr); /**< contructor for T* */
    
    virtual ~unique_ptr();

    unique_ptr<T,D>& operator=(unique_ptr<T,D>& other);
    unique_ptr<T,D>& operator=(T* ptr);
    
    T & operator*() const; /**< Indirection operator for T */
    T* operator->() const; /**< Dereference operator for T */
    
    operator bool() const; /**< Casting to check if ptr is ok to be used */

    unique_ptr<T,D> & realease(); /**< method to realise this pointer*/
    const T* get() const; /**< Retrieves internal pointer */

private:
    friend class shared_ptr<T,D>;

    void free(); /**< frees this ptr setting _ptr to null */
    //TODO think about a alloc() operator to automatize _count and _ptr

    T* _ptr; /**< The pointer the the class that this pointer represents */
};

#include "upointer-inl.hpp"

#endif //__UPOINTER_HPP__
