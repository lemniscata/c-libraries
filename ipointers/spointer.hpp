/// \brief Header only shared_ptr template. 
/// It defines an intelligent pointer with reference count.
/// This file only has definitions, the actual implementation 
/// is in spointer-inl.hpp file, that is included here.

/// \author: Pablo Cabeza García

#ifndef __SPOINTER_HPP__
#define __SPOINTER_HPP__

//Define NULL in case it is not defined, when no other header is included.
#ifndef NULL
    #define NULL 0
#endif

#include "iptrfwd.hpp"
//#include "upointer.hpp"
#include "deleter.hpp"

/**
 * shared_ptr class defines an intelligent pointes with reference
 * count, all major pointer operators are defined for this class.
 */
template<class T,class D>
class shared_ptr{
public:    
    shared_ptr(); /**< Default constructor, sets everything to null */
    shared_ptr(const shared_ptr<T,D> & ptr);  /**< Copy constructor */
    shared_ptr(T* ptr); /**< Used as contructor and assignment operator */

    /** Builds a shared_ptr setting ptr to NULL */
    explicit shared_ptr(unique_ptr<T,D>& ptr); 

    virtual ~shared_ptr();

    shared_ptr<T,D>& operator=(const shared_ptr<T,D>& other);
    
    T& operator*() const; /**< Indirection operator for T */
    T* operator->() const; /**< Desreference operator for T */
    
    operator bool() const; /**< Casting to check if ptr is ok to be used */

    shared_ptr<T,D>& realease(); /**< method to realise this pointer*/
    const T* get() const; /**< retrieves internal pointer */ 

private:
    void free(); /**< frees ptr or decrements reference count */
    //TODO think about a alloc() operator to automatize _count and _ptr

    T* _ptr; /**< The pointer the the class that this pointer represents */
    int* _count; /**< Reference count for _ptr */
};


#include "spointer-inl.hpp"


#endif //__SPOINTER_HPP__
