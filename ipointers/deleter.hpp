#ifndef __DELETER_HPP__
#define __DELETER_HPP__

template<class T>
class deleter{
public:
    void operator()(T* d){
	delete d;
    }
};

template<class T>
class Adeleter{
public:
    void operator()(T* d){
	delete [] d;
    }
};

#endif //__DELETER_HPP__
