// Actual implementation of unique_ptr template. It is included
// in upointer.hpp and should only be used from there.

// Author: Pablo Cabeza García

#ifndef __UPOINTER_INL_HPP__
#define __UPOINTER_INL_HPP__

#ifdef __UPOINTER_HPP__


template <class T,class D>
unique_ptr<T,D>::unique_ptr()
    :_ptr(NULL){}


template <class T, class D>
unique_ptr<T,D>::unique_ptr(unique_ptr<T,D> & ptr)
    :_ptr(ptr._ptr){
    ptr._ptr=NULL;
}


template <class T, class D>
unique_ptr<T,D>::unique_ptr(T* ptr)
    :_ptr(ptr){}


template <class T, class D>
unique_ptr<T,D>::~unique_ptr(){
    free();
}


template <class T, class D>
unique_ptr<T,D>& unique_ptr<T,D>::operator=(unique_ptr<T,D>& other){
    free();

    _ptr=other._ptr;
    other._ptr=NULL;

    return *this;
}


template <class T, class D>
unique_ptr<T,D>& unique_ptr<T,D>::operator=(T* ptr){
    free();

    _ptr=ptr;

    return *this;
}


template <class T, class D>
T& unique_ptr<T,D>::operator*() const{
    return *_ptr;
}


template <class T, class D>
T* unique_ptr<T,D>::operator->() const{
    return _ptr;
}


template <class T, class D>
unique_ptr<T,D>::operator bool() const{
    if(_ptr==NULL) return false;
    return true;
}


template <class T, class D>
unique_ptr<T,D> & unique_ptr<T,D>::realease(){
    free();
    return *this;
}


template <class T, class D>
const T* unique_ptr<T,D>::get() const{
    return _ptr;
}


template <class T, class D>
void unique_ptr<T,D>::free(){
    if(_ptr!=NULL){
	//D deletor;
	//deletor(_ptr);
	delete _ptr;
	_ptr=NULL;
    }
}

#endif //__UPOINTER_HPP__

#endif //__UPOINTER_INL_HPP__
