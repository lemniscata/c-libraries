#ifndef __IPTRFWD_HPP__
#define __IPTRFWD_HPP__


template<class T> class deleter;

template<class T,class D=deleter<T> > class shared_ptr;

template<class T,class D=deleter<T> > class unique_ptr;

#endif // __IPTRFWD_HPP__
