// This header wraps all *pointer headers in one to be used as and easy
// to include library. All pointer libraries are header only, template
// libraries.

// Author: Pablo Cabeza García

#ifndef __IPOINTERS_HPP__
#define __IPOINTERS_HPP__


#include "upointer.hpp" //unique_ptr class
#include "spointer.hpp" //shared_ptr class


#endif //__IPOINTERS_HPP__
