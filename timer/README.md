# Timer

Author: Pablo Cabeza

Version: 1.0


Intended cross-platform timer class that support 2 kind of time
formats:

- USER time
- KERNEL time

It could be used as a chronometer with its `start()`, `pause()`,
`stop()` methods.  But It also returns current time using `gettime()`.

To build it use `make XXX`, where XXX is one of the systems supported,
and it will build an static and a dynamic lib you can link to.

**Note**: This library makes use of my own intelligent pointers. To use it
without them, some changes are needed.
