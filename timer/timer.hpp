/**
 * File: timer.h
 * Author: Pablo Cabeza García
 * Constants: NSEC_PER_SEC, NSEC_PER_MSEC
 * Classes: timer
 */

#ifndef __TIMER_H
#define __TIMER_H

#include <ipointers/ipointers.hpp>

//Constants for conversions from various times
namespace ctimer{
    const long long int NSEC_PER_SEC=1000000000LL;
    const long long int NSEC_PER_MSEC=1000000LL;
}


/**
 * This timer class implements time related functionalities.
 * One way to use it is as a high precision cronometer. It is used
 * the same way as a real cronometer:
 *    - start() starts counting.
 *    - pause() returns the time passed from previous start and
 *              stops time count.
 *    - stop()  stops cronometer,returns whole time count and sets
 *              it to start counting again.
 * It has other useful methods that can be used from anywhere.
 */
class timer{
public:
    /** Enum to choose which time to use */
    enum ttype_t {
	USER, /**< USER retrieves user time */
	KERNEL /**< KERNEL retrieves the real kernel time */
    };

    /** In normal units sets precision */
    enum tprec_t {
	NANO, /**< Nanoseconds. */
	MILLI, /**< Milliseconds. */
	SEC /**< Seconds. */
    }; 

    bool start(); /**< cronometer alike start */
    double pause(); /**< cronometer alike pause */
    double stop(); /**< cronometer alike stop */

    /** Retrieves actual time as used by the system */
    static long long int gettime();

    /** Retrieves time in the form of string */
    static char * getstrtime();

    /** sleep thread sec aproximately */
    static void sleep(int sec); 
    
    /** sleep thread sec*NSEC_PER_SEC + nsec nanoseconds ( precision sleep */
    static void sleep(int sec,long int nsec);


    /** Constructor that sets units to NORMAL (default constractor) */
    timer(ttype_t tt=USER, tprec_t tp=NANO);

    virtual ~timer();

private:
    class timerimpl;
    unique_ptr<timerimpl> _impl;
};

#endif
