// Cross platform method and handler timerimpl of timer.hpp
// For each platform getusertime, getkerneltime and transform
// should be implemented in a file named timer.XXX.cpp where
// XXX is a system specific id

// Author: Pablo Cabeza

#include "timer.hpp"
#include "timer.pimpl.hpp"

#include <ctime>

// ==  General timer implementations  ==

timer::timer(timer::ttype_t tt,timer::tprec_t tp)
    :_impl(new timer::timerimpl(tt,tp)){}


timer::~timer(){}


/** It checks if not started jet, if so it just returns false. */
bool timer::start(){
    if(_impl->_beg==0){
	if(_impl->_type==timer::USER) _impl->_beg=_impl->getusertime();
	else _impl->_beg=_impl->getkerneltime();
	return true;
    }
    return false;
}


/** It gets the difference between now and start and sets start time to 0*/
double timer::pause(){
    long long int aux;
    
    if(_impl->_beg!=0){

	if(_impl->_type==timer::USER) _impl->_end=_impl->getusertime();
	else _impl->_end=_impl->getkerneltime();
	
	_impl->_tdiff+=(aux=( _impl->_end - _impl->_beg));
	_impl->_beg=0;
	return _impl->transform(aux,_impl->_prec);
    }
    return -1;
}


/** 
 * Calls pause if it haven't been called , returns total time between each
 * pause, start call, and resets timer.
 */
double timer::stop(){
    double aux;
    if(_impl->_beg!=0) pause();
    
    aux=_impl->transform(_impl->_tdiff,_impl->_prec);
    _impl->_tdiff=0;
    return aux;
}


long long int timer::gettime(){
    return time(NULL);
}


char * timer::getstrtime(){
    time_t t=time(NULL);
    return ctime(&t);
}

void timer::sleep(int sec){
    timerimpl::sleep(sec);
}

 
void timer::sleep(int sec,long int nsec){
    timerimpl::precsleep(sec*ctimer::NSEC_PER_SEC+nsec);
}
