#ifndef __TIMER_PIMPL_HPP__
#define __TIMER_PIMPL_HPP__

#include "timer.hpp"

/**
 * Definition of the actual implementation of the methods from the timer
 */
class timer::timerimpl{
public:
    typedef long long int ptime_t;
    
    timerimpl(timer::ttype_t tt,timer::tprec_t tp);

    ptime_t getusertime(); /**< Retrieves user time */  
    ptime_t getkerneltime(); /**< Retrieves kernel time */

    /** Returns the value of ptime_t with the desired config, "tt" and "tp" */
    double transform(ptime_t t,timer::tprec_t tp);

    static void sleep(int sec); /**< sleeps a thread sec seconds */
    static void precsleep(long int nsec); /**< sleeps a thread nsec nanoseconds */

    timer::ttype_t _type;  /**< User or Kernel time */
    timer::tprec_t _prec; /**< Precision of the timer */
    
    ptime_t _beg;
    ptime_t _end;
    ptime_t _tdiff;
};


#endif //__TIMER_PIMPL_HPP__
