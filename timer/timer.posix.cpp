#include <unistd.h>
#include <time.h>
#include <sys/times.h>

#include "timer.hpp" 
#include "timer.pimpl.hpp"


namespace ctimer {
    int TICK_PER_SEC=-1;
}


//  ==  Methods for timer::timerimpl in posix systems  ==
timer::timerimpl::timerimpl(timer::ttype_t tt,timer::tprec_t tp)
    :_type(tt),
     _prec(tp),
     _beg(0),
     _end(0),
     _tdiff(0){}

/** 
 * It returns time in ticks and transforms it to nanoseconds before
 * returning it. It uses <sys/times.h> to retrieve user time.
 */
timer::timerimpl::ptime_t timer::timerimpl::getusertime(){
    tms aux;
    times(&aux);
    if( ctimer::TICK_PER_SEC==-1 ) ctimer::TICK_PER_SEC=sysconf(_SC_CLK_TCK);
    return (ctimer::NSEC_PER_SEC*aux.tms_utime)/ctimer::TICK_PER_SEC;
}


/**
 * It returns time in nanoseconds by default. To meassure time it
 * uses HPET ( high precision event timer) in <time.h> with flags -lrt
 */
timer::timerimpl::ptime_t timer::timerimpl::getkerneltime(){
    timespec aux;
    clock_gettime(CLOCK_MONOTONIC, &aux);
    return (ptime_t)aux.tv_sec*ctimer::NSEC_PER_SEC + aux.tv_nsec;
}


/** Transforms ptime_t t into the precition especified */
double timer::timerimpl::transform(timerimpl::ptime_t t, 
				   timer::tprec_t tp){
    ptime_t div=1;
    if(tp==timer::MILLI) div= ctimer::NSEC_PER_MSEC;
    else if(tp==timer::SEC) div= ctimer::NSEC_PER_SEC;
    
    return ( (double)t ) / div;
}


void timer::timerimpl::sleep(int sec){
    //TODO implement it
}


void timer::timerimpl::precsleep(long int nsec){
    //TODO implement it
}
