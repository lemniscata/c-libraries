#ifndef __SOCKET_HPP__
#define __SOCKET_HPP__

#include "socket_t.interface.hpp"

#include <string>

#include <idioms/noncopyable.hpp>
#include <ipointers/upointer.hpp>


using std::string;

namespace lmn{

/**
   socket class

   StreamPolicy is intended to implement the long term buffering
   functionality of the socket. It should contain:

   	-- R1 readData(char* buffer,int size)
	-- R2 getInnerBuffer()
	-- constructor without parameters

   where R1, R2 can be any type.
*/
template <class StreamPolicy,int bufferSize=1024>

class socket
    :public StreamPolicy,
     private noncopyable<socket<StreamPolicy,bufferSize> >{

    // StreamPolicy required methods
    using StreamPolicy::readData;
    using StreamPolicy::getInnerBuffer;

public:

    socket();
    explicit socket(socket_t* sckt);
    explicit socket(unique_ptr<socket_t> sckt);

    /** connects to dir onto port, it should resolve dir */
    bool connect(string& dir, int port);  
    
    bool close(); /**< closes connection if available */



    int read(); /**< It delegates on StreamPolicy's own
		    readData(char*) to stream data. It reads until
		    fail */

    int read(int bytes); /**< reads until bytes are read or until
			    fail */

    int read(char* buffer,int size); /**< Reads to buffer up to size */



    int write(const string& str); /**< Writes the whole string or until
			       the connection is broken.  */

    int write(const char* buffer,int size); /**< Writes from buffer up to
					 size bytes */

    operator bool() const; /**< tells wether connection is available or not */
	
private:
    /** 
	Actual implementation, platform dependant, of socket
	functionalities. (piml idiom)
    */
    unique_ptr<socket_t> _socket;
    char _buffer[bufferSize];

}; // class socket<StreamPolicy,bufferSize>


} // namespace lmn


#include "socket-inl.hpp"


#endif // __SOCKET_HPP__
