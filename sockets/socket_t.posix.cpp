#include "socket_t.interface.hpp"

#include <cstring>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <iostream>
using namespace std;

using std::string;

namespace lmn{


struct socket_t::connectionData{
    int sockfd;
    sockaddr_in servaddr;
    hostent *server;

    connectionData()
	:sockfd(-1),
	 server(NULL){
	memset(&servaddr,0,sizeof(servaddr));
    }
};



socket_t::socket_t()
    :_data(new connectionData){}


socket_t::~socket_t(){
    if(_data!=NULL && _data->sockfd>=0)
	close();
    delete _data;
}


void socket_t::swap(socket_t& s){
    connectionData* aux=s._data;
    s._data=_data;
    _data=aux;
}


socket_t& socket_t::operator=(socket_t s){
    s.swap(*this);
    s._data->sockfd=-1;
    return *this;
}


bool socket_t::connect(const string& dir,int port ){
    if(_data->sockfd>=0) close();

    _data->sockfd=socket(AF_INET,SOCK_STREAM,0);
    if(_data->sockfd<0) return false;

    _data->server=gethostbyname(dir.c_str()); //Resolve direction
    if(_data->server==NULL) return false;

    _data->servaddr.sin_family= AF_INET;
    
    memcpy((char*)&_data->servaddr.sin_addr.s_addr,
	   (char*)_data->server->h_addr,
	   _data->server->h_length);
    
    _data->servaddr.sin_port=htons(port);
    return 0 <= ::connect(_data->sockfd,
			  (sockaddr*)(&_data->servaddr),
			  sizeof(_data->servaddr));  
}


bool socket_t::close(){
    if(_data->sockfd >= 0)
	::close(_data->sockfd);
    *_data=connectionData();
    return true;
}


int socket_t::write(const char* buff,int size){
    return ::write(_data->sockfd,buff,size);
    //TODO handle errors??
}


int socket_t::read(char* buff,int size){
    return ::read(_data->sockfd,buff,size);
    //TODO handle errors??
}


socket_t::operator bool() const{
    return _data!=NULL &&  _data->sockfd>=0;
}


} // namespace lmn
