#ifndef __SOCKET_T_INTERFACE_HPP__
#define __SOCKET_T_INTERFACE_HPP__

#include <string>

using std::string;

namespace lmn{

/**
   The implementation of socket_t is system dependant
 */
class socket_t{
public:
    socket_t();
    socket_t(const socket_t& sock);
    virtual ~socket_t();

    void swap(socket_t& s);
    socket_t& operator=(socket_t s);

    /** writes a string to the socket */
    int write(const char* str,int size); 
    
    /** reads from the socket to stream */
    int read(char * buffer, int size); 

    /**< connects to dir using port */
    bool connect(const string& dir, int port); 

    bool close(); /**< closes connection if possible */
    
    operator bool() const; /**< wether socket is valid or not */

private:
    struct connectionData;
    connectionData* _data;
};

} // namespace lmn

#endif // __SOCKET_T_INTERFACE_HPP__
