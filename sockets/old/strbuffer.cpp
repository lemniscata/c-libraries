#include "strbuffer.hpp"

#include <cstring>
#include <string>

namespace bfr{

/** 
    It uses COW techniques to speed up creation, 
    thus sets everything to NULL	
*/
buffer::buffer(int size)
    :_size(size),_buffer(NULL),_head(NULL),_tail(NULL){}


/** This copies a buffer in the most efficient way (trying not to alloc)*/
buffer::buffer(const buffer& other){
    _buffer=new char[other._size];
    _size=other._size;

    memcpy( _buffer , other._head , other._tail-other._head );
    _head=_buffer;
    _tail=_head+(other._tail-other._head);
}

#ifdef __GXX_EXPERIMENTAL_CXX0X__

/** It reaps resources (buffer) from other */
buffer::buffer(buffer && other)
    :_size(other._size),_head(other._head),_tail(other._tail){
    //TODO improve it to safe the old buffer to use it later
    //delete[] _buffer; buffer is being created...
    _buffer=other._buffer;
	
    other._buffer=NULL; 
}

#endif

buffer::~buffer(){
    delete[] _buffer;
}



buffer& buffer::operator=(buffer& other){
    if(_size< (unsigned int)(other._tail-other._head)){ 
	delete[] _buffer;
	_buffer=new char[other._size];
	_size=other._size;
    }
    else if(_buffer==NULL){ // if _buffer is not jet created (COW)
	_buffer=new char[other._size];
	_size=other._size;
    }

    memcpy( _buffer , other._head , other._tail-other._head );
    _head=_buffer;
    _tail=_head+(other._tail-other._head);
}

#ifdef __GXX_EXPERIMENTAL_CXX0X__
buffer& buffer::operator=(buffer&& other){
    delete[] _buffer;
    _buffer=other._buffer;
    _head=other._head;
    _tail=other._tail;
    _size=other._size;
    return *this;
}
#endif


char* buffer::internalbuf(){
    return _buffer;
}

    
unsigned int buffer::bufsize(){
    return _size;
}


char* buffer::rpointer(pos_t pos){
    if(pos==beg) return _head;
    else return _tail;
}


unsigned int buffer::size(){
    return (unsigned int) (_tail-_head);
}

void buffer::read(string& str){
    if(_head==NULL){
	str=string();
    }	
    else{
	// eliminate leading whites
	while(_head!=_tail && std::isspace(*_head)) ++_head;
	char* beg=_head;

	while(_head!=_tail && !std::isspace(*_head)) ++_head;
	
	if(_head==_tail) str=string(_head,_tail);
	else str=string(beg,_head++);
    }
}


string buffer::read(){
    string aux;
    read(aux);
    return aux;
}


buffer& buffer::operator>>(string& str){
    read(str);
    return *this;
}


void buffer::write(const char* str){
    unsigned int size=strlen(str);
    create_on_write();
    if(size>_size-buffer::size()){
	
	unsigned int nsize=_size;
	do nsize*=2;
	while(nsize<size);

	do_resize(nsize);
    }
    else if(_size-(_tail-_buffer)<size){
	defrag();
    }
    memcpy(_tail,str,size);
    _tail+=size;
}

void buffer::write(const string& str){
    write(str.c_str());
}

buffer& buffer::operator<<(const string& str){
    write(str.c_str());
    return *this;
}
	

void buffer::resize(unsigned int size){
    do_resize(size);
}


void buffer::create_on_write(){
    if(_buffer==NULL){
	_buffer=new char[_size];
	_head=_tail=_buffer;
    }
}


void buffer::do_resize(unsigned int size){
    char* buff=_buffer;
    _buffer=new char[size];
    _size=size;

    int num=(buffer::size()>size)?size:buffer::size(); 
    memcpy(_buffer,_head,num);

    _head=_buffer;
    _tail=_head+num;

    delete[] buff;
}


void buffer::defrag(){
    if((unsigned int)(_head-_buffer)>=size())
	memcpy(_buffer,_head,size());
    else
	memmove(_buffer,_head,size());
    _tail=_buffer+size();
    _head=_buffer;
}


} //namespace bfr
