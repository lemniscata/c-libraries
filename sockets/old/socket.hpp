#ifndef __SOCKET_HPP__
#define __SOCKET_HPP__

#include <sstream>
#include <iostream>
#include "socketstream.hpp"



class socket
    :public socketstream{

public:	
    socket():socketstream(NULL){}
    socket(string dir,int port):socketstream(bsocket_t(dir,port)){}
    socket(socket_t * s):socketstream(s){}

    socket& connect(string address,int port=-1);
    socket& close();
    bool connected();
};


class server{
public:
    socket& bind(int port=-1);
    socket& listen();
    socket accept(); //TODO use mojo


}

#endif
