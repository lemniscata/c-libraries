#ifndef __SOCKET_T_POSIX_HPP__
#define __SOCKET_T_POSIX_HPP__

#include <sys/types.h>
#include <sys/socket.h>

/** class that represents a socket in posix environments */
class socket_t{
public:
    socket_t(); /**< It builds sockd to an invalid value */
    
    bool write(string & str); /**< writes a string to the socket */
    bool read(stringstream& stream); /**< reads from the socket to stream */
    
private:
    int sockd;
};



#endif //__SOCKET_T_POSIX_HPP
