#include "socketstream.hpp"

namespace skt{
    socketstream& flush(socketstream & s){
	return s.flush();
    }

    socketstream& endl(socketstream & s){
	return (s<<"\n").flush();
    }

} //namespace skt




//TODO do it as a moving constructor
socketstream::socketstream(socket_t* t):_socket(t){}


//TODO use a unique_ptr -> no need for delete
socketstream::~socketstream(){
    this->flush();
    delete socket_t;
}


// === Operators === 

socketstream& socketstream::operator<<(::manip_t m){
    _isstream << m;
    return *this;
}

socketstream& socketstream::operator<<(smanip_t m){
    return m(*this);
}




//  === operations to the socket_t ===
// This operations relies on the system-specific implementations from socket_t

socketstream& socketstream::write(const string& str){
    _socket->write(str);
    return this*
}

socketstream& socketstream::read(){
    _socket->read(_osstream);
    return *this;
}

socketstream& socketstream::flush(){
    _socket->write(_isstream.str());
    _isstream.clear();
    return *this;
}
