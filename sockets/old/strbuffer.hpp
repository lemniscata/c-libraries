#ifndef __STRBUFFER_HPP__
#define __STRBUFFER_HPP__

#include <cctype>
#include <string>
using std::string;

namespace bfr{
    
    class buffer{
    public:
	enum pos_t { beg , end };

	explicit buffer(int size=1000);
	buffer(const buffer& other);

	#ifdef __GXX_EXPERIMENTAL_CXX0X__
	buffer(buffer && other);
	#endif

	virtual ~buffer();

	buffer& operator=(buffer& other);
	#ifdef __GXX_EXPERIMENTAL_CXX0X__
	buffer& operator=(buffer&& other);
	#endif

	char* internalbuf();
	unsigned int bufsize();
	
	char* rpointer(pos_t pos=beg);
	unsigned int size();

	void read(string& str);
	string read();
	buffer& operator>>(string& str);

	void write(const string& str);
	void write(const char* str);
	buffer& operator<<(const string& str);
	
	void resize(unsigned int size);
	
    private:
	
	void create_on_write(); /**< check if _buffer is ready to
				   write */

	
	void do_resize(unsigned int size); /**< allocates new memory
					      and  moves everything to it */
	
	void defrag(); /**< auxiliary, used to set internal buffer to be ok */

	unsigned int _size;
	char* _buffer;

	char* _head;
	char* _tail;		
    };

} //namespace bfr

#endif // __STRBUFFER_HPP__
