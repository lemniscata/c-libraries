#ifndef __SOCKETSTREAM_HPP__
#define __SOCKETSTREAM_HPP__

namespace {

    typedef std::ostream&(*manip_t)(std::ostream&);

} //namespace



namespace skt{

    socketstream& flush(socketstream & s);
    socketstream& endl(socketstream & s);

} //namespace skt




/**
   socketstream is a stream that is able to write and read from a socket,
   to build it you must first provide a socket
 */
class socketstream{
public:
    typedef socketstream&(*smanip_t)(socketstream&);

    friend socketstream& skt::flush(socketstream&);
    friend socketstream& skt::endl(socketstream&);

    virtual ~socketstream();

    socketstream& read(string str);
    socketstream& write(string str);
    socketstream& flush();

    template <class T> socketstream& operator<<(T & ref);
    socketstream & operator<<(::manip_t m);
    socketstream & operator<<(smanip_t m);

    template <class T> socketstream& operator>>(T & ref);
    operator bool(); /**< Check if socketstream is ready to read and write */

protected:
    explicit socketstream(socket_t* t);

private:
    std::stringstream _isstream; /**< socket <- user */
    std::stringstream _osstream; /**< socket -> user */

    /**
       It must contain:
         --read(stringstream&)
	 --write(string &)
     */
    struct socket_t;

    //TODO use unique_ptr
    socket_t* _socket;
};



template <class T>
socketstream& socketstream::operator<< <T>(T & ref){
    _isstream<<ref;
    return *this;
}


template <class T> socketstream& operator>>(T & ref){
    if(_ostream.empty()) //DEBUG
	//TODO	
};



#endif //__SOCKETSTREAM_HPP__
