#ifndef __SOCKET_INL_HPP__
#define __SOCKET_INL_HPP__

#ifdef __SOCKET_HPP__


namespace lmn{


template <class StreamPolicy,int bufferSize>
socket<StreamPolicy,bufferSize>::socket()
    :_socket(new socket_t()){}


template <class StreamPolicy,int bufferSize>
socket<StreamPolicy,bufferSize>::socket(socket_t* sckt)
    :_socket(sckt){}


template <class StreamPolicy,int bufferSize>
socket<StreamPolicy,bufferSize>::socket(unique_ptr<socket_t> sckt)
    :_socket(sckt){}


template <class StreamPolicy,int bufferSize>
bool socket<StreamPolicy,bufferSize>::connect(string& dir, int port){
    if(*_socket) _socket->close();
    return _socket->connect(dir,port);
}


template <class StreamPolicy,int bufferSize>
bool socket<StreamPolicy,bufferSize>::close(){
    if(*_socket) return _socket->close();
    return false;
}


template <class StreamPolicy, int bufferSize>
int socket<StreamPolicy,bufferSize>::read(){
    int bytes=0;
    int readb;
    while( (readb=_socket->read(_buffer,bufferSize)) == bufferSize ){
	readData(_buffer,bufferSize);
	bytes+=bufferSize;
    }
    readData(_buffer,readb);
    return bytes+readb;
}



template <class StreamPolicy, int bufferSize>
int socket<StreamPolicy,bufferSize>::read(int bytes){
    int readb,total=bytes;
    while(bytes > bufferSize){
	readb=_socket->read(_buffer,bufferSize);
	readData(_buffer,readb);
	
	if(readb < bufferSize)
	    return total-bytes+readb;
	
	bytes-=readb;
    }
    readb=_socket->read(_buffer,bytes);
    readData(_buffer,readb);
    return total-bytes+readb;
}


template <class StreamPolicy, int bufferSize>
int socket<StreamPolicy,bufferSize>::read(char* buffer,int size){
    return _socket->read(buffer,size);
}



template <class StreamPolicy, int bufferSize>
int socket<StreamPolicy,bufferSize>::write(const string& str){
    return _socket->write(str.c_str(),str.size());
}


template <class StreamPolicy, int bufferSize>
int socket<StreamPolicy,bufferSize>::write(const char* buffer,int size){
    return _socket->write(buffer,size);
}


template <class StreamPolicy, int bufferSize>
socket<StreamPolicy,bufferSize>::operator bool() const{
    return *_socket;
}


} // namespace lmn

#endif // __SOCKET_HPP__

#endif // __SOCKET_INL_HPP__
