/// \brief functors


#ifndef __FUNCTORS_HPP__
#define __FUNCTORS_HPP__

#include <ipointers/ipointers.hpp>
#include <utils/ref.hpp>

namespace utils{


/**
   Interface for the actual functor without parameters. It is later
   used by functor class an an entry point.
*/
class handler{
public:
    virtual void operator()()=0;
};



/**
   Class that implements handler interface, it is the actual functor
   for functions with none parameter. It contains an instance of T to
   call it.
 */
template <class T>
class binder
    :public handler{
public:

    binder(T h);
    void operator()();

private:
    T _handler;  
};



/*
  Class that wraps a reference<T> to use it as a functor 
*/
template <class T>
class aux_reference
    :public reference<T>{

public:
    explicit aux_reference(reference<T>& r);
    void operator ()();
};


/**
   general non-parameter functor class. functor class uses a handler
   object to be the actual functor and calls it.
*/
class functor{
public:
    functor();

    template<class T> explicit functor(T b);
    template<class T> explicit functor(reference<T> b);

    void operator()();
    operator bool();

private:
    shared_ptr<handler> _handler;
};



} // namespace utils

#include "functors-inl.hpp"

#endif // __FUNCTORS_HPP__


