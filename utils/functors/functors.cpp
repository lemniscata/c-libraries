#include "functors.hpp"

namespace utils{


// [begin] class functor

functor::functor(){}
    
void functor::operator()(){
    if(_handler) (*_handler)();
}


functor::operator bool(){
    return _handler;
}

// [end] class functor


} // namespace utils
