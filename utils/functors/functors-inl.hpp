#ifndef __FUNCTORS_INL_HPP__
#define __FUNCTORS_INL_HPP__

#ifdef __FUNCTORS_HPP__


namespace utils{


// [begin] class binder<T>

template <class T>
binder<T>::binder(T h):_handler(h){}
    
template <class T>
void binder<T>::operator()(){
    _handler();
}

// [end] class binder<T>


// [begin] class aux_reference<T>

template <class T>
aux_reference<T>::aux_reference(reference<T>& r)
 :reference<T>(r){}
    

template <class T>
void aux_reference<T>::operator ()(){
    reference<T>::_ref();
}

// [end] class aux_reference<T>


// [begin] class functor

template<class T>
functor::functor(T b):_handler(new binder<T>(b)){}


template<class T>
functor::functor(reference<T> b)
    :_handler(new binder<aux_reference<T> >(aux_reference<T>(b))){}

// [end] class functor

	
} // namespace utils


#endif // __FUNCTORS_HPP__
#endif // __FUNCTORS_INL_HPP__
