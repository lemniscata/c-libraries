/// @brief Header for string utils library.
/// It is suposed to hold useful operations with strings that
/// are not considered in std or stl.

/// @author: Pablo Cabeza García

#ifndef __STRINGUTILS_HPP__
#define __STRINGUTILS_HPP__

#include <string>
#include <vector>

/** namespace for STRing Utils to avoid collisions */
namespace stru{


    /** 
     * Replaces all ocurrences of a certain string in another, could specify a
     * number of ocurrences.
     * @param str the from which to replace
     * @param find the substring to find and replace
     * @param rep the replacement for each string
     * @param max number of ocurrences to be replaced, if string::npos, all
     * @return true if any was replaced, false otherwise
     */
    bool replaceall(std::string &str, 
		    const std::string &find, 
		    const std::string &rep, 
		    unsigned int max = std::string::npos); 


    /**
     * Finds all instances of a substring in a string.
     * There are two modes of working, if superposition is true, then
     * it will search all instances of find as substring, otherwise,
     * if one is found it will jump over it.
     * @param str the string that will be searched in
     * @param find the substring to search for
     * @param superposition whether the search for new words jumps the last.
     * @return a vector with the positions of the substrings
     */
    std::vector<unsigned int> findall(const std::string &str,
				      const std::string &find,
				      bool superposition=false);


    /**
     * Turns to uppercase all characters in a string, if possible.
     * @param str the string to be transform
     * @return a reference to the string.
     */
    std::string & toupper(std::string & str);
   

    /**
    * Turns to lowercase all characters in a string, if possible.
    * @param str the string to be transform
    * @return a reference to the string
    */
    std::string & tolower(std::string & str);


    /**
     * It returns am uppercase copy of a string
     * @param str the string to be copyed
     * @return the uppercase copy from the original string
     */
    std::string uppercpy(std::string str);


    /**
     * It returns a lowercase copy of a string
     * @param str the string to be copyed
     * @return the lowercase copy from the original string
     */
    std::string lowercpy(std::string str);
    

} //namespace stru

#endif //__STRINGUTILS_HPP__
