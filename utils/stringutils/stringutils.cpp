#include "stringutils.hpp"

#include <string>
#include <vector>
#include <cctype>

using std::vector;
using std::string;


namespace stru{

    bool replaceall(std::string & str, const std::string & find,
		    const std::string & rep, unsigned int max){

	max= (max==string::npos)?-1:max;
	unsigned int count=0, pos=0, offset=0;

	while( max != count && 
	      (pos = str.find(find,offset)) != string::npos){

	    str.replace(pos,find.length(),rep);
	    offset+=rep.length();
	    ++count;
	}
	
	return static_cast<bool>(count);
    }



    vector<unsigned int> findall(const string &str, const string &find,
				 bool superposition){
	vector<unsigned int> v;
	unsigned int offset= superposition ? 1 : find.length(),
	    pos=0;

	while( (pos=str.find(find,pos)) != string::npos ){
	    v.push_back(pos);
	    pos+=offset;
	}

	return v;
    }



    std::string & toupper(std::string & str){
	int L=str.length();
	char c;
	
	for(int i=0;i<L;i++) str[i]=std::toupper(str[i]);
	
	return str;
    }


    std::string & tolower(std::string & str){
	int L=str.length();
	char c;
	
	for(int i=0;i<L;i++) str[i]=std::tolower(str[i]);
	
	return str;
    }

    

    std::string uppercpy(std::string str){
	return toupper(str);
    }



    std::string lowercpy(std::string str){
	return tolower(str);
    }



} //namespace stru
