#ifndef __REF_HPP__
#define __REF_HPP__


namespace utils{


template <class T>
class reference{
public:
    explicit reference(T& o);
    
    operator T&();
    T& _ref;
};


template <class T>
reference<T> ref(T& r);


} //namespace utils


#include "ref-inl.hpp"

#endif // __REF_HPP__
