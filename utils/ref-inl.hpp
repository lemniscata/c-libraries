#ifndef __REF_INL_HPP__
#define __REF_INL_HPP__

#ifdef __REF_HPP__

namespace utils{

// [begin] class reference

template <class T>
reference<T>::reference(T& o):_ref(o){}

template <class T>    
reference<T>::operator T&(){
    return _ref;
}

// [end] class reference


template <class T>
reference<T> ref(T& r){
    return reference<T>(r);
}


} //namespace utils

#endif // __REF_HPP__
#endif // __REF_INL_HPP__
