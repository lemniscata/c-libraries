#ifndef __MOJO_HPP__
#define __MOJO_HPP__

namespace mojo{

    template <class T>
    struct constant{
	const T* _data;
    public:
	explicit constant(const T & obj): _data(&obj){}
	const T & get() const{
	    return *_data;
	}
    };
    
    template <class T>
    class temporary: private constant<T>{
    public:
	explicit temporary(T& obj): constant<T>(obj){}
	T & get() const{
	    return const_cast<T &>(constant<T>::get());
	}
    };

    template <class T>
    class fnresult:public T{
    public:
	fnresult(const fnresult& rhs)
	    :T( temporary<T>(const_cast<fnresult&>(rhs))){}

	explicit fnresult(T& rhs)
	    :T( temporary<T>(rhs)){}
    };
    

    template <class T>
    class enabled{
    public:
	operator temporary<T>(){
	    return temporary<T>(static_cast<T&>(*this));
	}

	operator constant<T>() const{
	    return constant<T>(static_cast<const T&>(*this));
	}

	operator fnresult(){
	    return fnresult<T>(static_cast<T&>(*this));
	}
	
    protected:
	enabled(){}
	~enabled(){}
    };

}

#endif
